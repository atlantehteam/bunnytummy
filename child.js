
(function($) {
    $(document).on('click', '.bt_measure_wrapper .measure_btn', function() {
        const $btn = $(this);
        const itemId = $btn.attr('data-item-id');
        const $wrapper = $btn.closest('.bt_measure_wrapper')
        $wrapper.find(`.measure_view_item`).removeClass('bt-active');
        $wrapper.find(`.measure_btn`).removeClass('bt-active');
        $wrapper.find(`.measure_view_item[data-item-id="${itemId}"]`).addClass('bt-active')
        $btn.addClass('bt-active')
    })

    $('form.variations_form.cart').submit(function() {
        const $btn = $(this).find('button.single_add_to_cart_button ');
        if ($btn.hasClass('disabled') || $btn.prop('disabled')) {
            return;
        }
        const text = $btn.text();
        $btn.prop('disabled', true)
        $btn.text('המוצר מתווסף לסל..')
        setTimeout(() => {
            $btn.text(text);
            $btn.prop('disabled', false);
        }, 5000);
    })

    $('.woocommerce').on('change', 'input.qty', function(){
        $("[name='update_cart']").trigger("click");
    });
    
    $('.bt_shop .jet-woo-product-thumbs.effect-fade').each(function() {
        $(this).closest('.jet-woo-builder-product').addClass('jet-woo-thumb-with-effect')
    })
})(jQuery)
