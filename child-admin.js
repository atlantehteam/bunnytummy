/*
(function($) {
    var settings = {
        setting_variation_image: null,
        setting_variation_image_id: null
    }
    function add_measure_image(event) {
        var $button = $( this ),
            post_id = $button.attr( 'rel' ),
            $parent = $button.closest( '.upload_measure_image' );

        settings.setting_variation_image    = $parent;
        settings.setting_variation_image_id = post_id;

        event.preventDefault();

        if ( $button.is( '.remove' ) ) {

            $( '.upload_measure_image_id', settings.setting_variation_image ).val( '' ).trigger( 'change' );
            settings.setting_variation_image.find( 'img' ).eq( 0 )
                .attr( 'src', woocommerce_admin_meta_boxes_variations.woocommerce_placeholder_img_src );
            settings.setting_variation_image.find( '.upload_measure_image_button' ).removeClass( 'remove' );

        } else {

            // If the media frame already exists, reopen it.
            if ( settings.variable_image_frame ) {
                settings.variable_image_frame.uploader.uploader
                    .param( 'post_id', settings.setting_variation_image_id );
                settings.variable_image_frame.open();
                return;
            } else {
                wp.media.model.settings.post.id = settings.setting_variation_image_id;
            }

            // Create the media frame.
            settings.variable_image_frame = wp.media.frames.variable_image = wp.media({
                // Set the title of the modal.
                title: woocommerce_admin_meta_boxes_variations.i18n_choose_image,
                button: {
                    text: woocommerce_admin_meta_boxes_variations.i18n_set_image
                },
                states: [
                    new wp.media.controller.Library({
                        title: woocommerce_admin_meta_boxes_variations.i18n_choose_image,
                        filterable: 'all'
                    })
                ]
            });

            // When an image is selected, run a callback.
            settings.variable_image_frame.on( 'select', function () {

                var attachment = settings.variable_image_frame.state()
                    .get( 'selection' ).first().toJSON(),
                    url = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

                $( '.upload_measure_image_id', settings.setting_variation_image ).val( attachment.id )
                    .trigger( 'change' );
                settings.setting_variation_image.find( '.upload_measure_image_button' ).addClass( 'remove' );
                settings.setting_variation_image.find( 'img' ).eq( 0 ).attr( 'src', url );

                wp.media.model.settings.post.id = settings.wp_media_post_id;
            });

            // Finally, open the modal.
            settings.variable_image_frame.open();
        }
    }

    $(document).on('click', '.upload_measure_image', add_measure_image);
})(jQuery)
*/