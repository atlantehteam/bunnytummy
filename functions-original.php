<?php
add_filter( 'woocommerce_default_address_fields' , 'change_checkout_address_fields' );

function change_checkout_address_fields( $address_fields ) {

	$address_fields['address_1']['placeholder'] = '';

	return $address_fields;
}
