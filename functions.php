<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/* code copied from override parent elementor before updating template */
function my_child_theme_styles(){
	wp_enqueue_style( 'checkout-css', get_stylesheet_directory_uri() . '/assets/css/checkout-page.css');
	add_thickbox();
}

add_action('wp_enqueue_scripts', 'my_child_theme_styles');
/* END - code copied from override parent elementor before updating template */

add_filter( 'woocommerce_default_address_fields' , 'change_checkout_address_fields' );

function change_checkout_address_fields( $address_fields ) {

	$address_fields['address_1']['placeholder'] = '';

	return $address_fields;
}

add_filter( 'wc_add_to_cart_message', 'remove_cart_message' );

function remove_cart_message() {
    return;
}

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */

/*
* This section check if the $directories array is not empty
* and than require all php files that these directories contain.
*/
if( !empty(array('inc')) ) {
   foreach(array('inc') as $directory) {
	   $dir = glob(dirname( __FILE__ ) . '/'.$directory.'/*.php');

	   foreach ($dir as $file)
		   require_once $file;
   }
}

function hello_elementor_child_enqueue_scripts() {
	$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/style.css'));
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[],
		$cache_buster
	);

		$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/child.js'));
		wp_enqueue_script( 'hello-elementor-child-script', get_stylesheet_directory_uri() . '/child.js', array('jquery'), $cache_buster, true);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );

// function hello_elementor_child_enqueue_scripts_admin() {
// 	$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/child-admin.js'));
// 	wp_enqueue_script( 'hello-elementor-admin-child-script', get_stylesheet_directory_uri() . '/child-admin.js', array('jquery'), $cache_buster, true);
// }
// add_action( 'admin_enqueue_scripts', 'hello_elementor_child_enqueue_scripts_admin', 20 );

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/* Disable Page Title */
function ele_disable_page_title( $return ) {
   return false;
}
add_filter( 'hello_elementor_page_title', 'ele_disable_page_title' );


//my-customization

// change text of the "see-options" btn

add_filter( 'woocommerce_product_add_to_cart_text', function( $text ) {
	global $product;
	if ( $product->is_type( 'variable' ) ) {
		$text = $product->is_purchasable() ? __( 'רכישה', 'woocommerce' ) : __( 'פרטים ', 'woocommerce' );
	}
	return $text;
}, 10 );

function eran_remove_quantity_fields( $return, $product ) {
    return true;
}

/*
// remove quantity fields

add_filter( 'woocommerce_is_sold_individually', 'eran_remove_quantity_fields', 10, 2 );
*/

// variations

add_filter( 'woocommerce_variable_sale_price_html', 'eran_variation_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'eran_variation_price_format', 10, 2 );
 
function eran_variation_price_format( $price, $product ) {
 
// Main Price
$prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
$price = $prices[0] !== $prices[1] ? sprintf( __( 'החל מ- %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
 
// Sale Price
$prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
sort( $prices );
$saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'החל מ- %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
 
if ( $price !== $saleprice ) {
$price = '<del>' . $saleprice . $product->get_price_suffix() . '</del> <ins>' . $price . $product->get_price_suffix() . '</ins>';
}
return $price;
}


// Remove breadcrumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

// add variation Swatch to jetwoo archive
/*

add_filter( 'jet-woo-builder/template-functions/product-price', 'get_wvs_pro_archive_variation_template', 999, 1 ); 

function get_wvs_pro_archive_variation_template( $html ) { 
		
		if ( is_product_taxonomy() || is_post_type_archive( 'product' ) || is_page( wc_get_page_id( 'shop' ) ) ) {
			$wvs_archive_variation_template = wvs_pro_archive_variation_template(); 
			$html .= $wvs_archive_variation_template; 

			return '<div class="price">' . $html . '</div>';
		}
		
		return $html;
		
	}
*/

//add new badge
/*
add_action( 'woocommerce_before_shop_loop_item_title', 'bbloomer_new_badge_shop_page', 3 );
          
function bbloomer_new_badge_shop_page() {
   global $product;
   $newness_days = 30;
   $created = strtotime( $product->get_date_created() );
   if ( ( time() - ( 60 * 60 * 24 * $newness_days ) ) < $created ) {
      echo '<span class="itsnew onsale">' . esc_html__( 'New!', 'woocommerce' ) . '</span>';
   }
}
  */

/*

// Filer WooCommerce Flexslider options - Add Navigation Arrows  

add_filter( 'woocommerce_single_product_carousel_options', 'eran_update_woo_flexslider_options' );

function eran_update_woo_flexslider_options( $options ) {

    $options['directionNav'] = true;

    return $options;
}
*/

add_action( 'wp','eran_remove_woocommerce_theme_support',99);

function eran_remove_woocommerce_theme_support() {
remove_theme_support( 'wc-product-gallery-zoom' );
}

add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );

/*
add_filter( 'woocommerce_loop_add_to_cart_link', 'ts_replace_add_to_cart_button', 10, 2 );
function ts_replace_add_to_cart_button( $button, $product ) {
if (is_product_category() || is_shop()) {
$button_text = __("רכישה", "woocommerce");
$button_link = $product->get_permalink();
$button = '<a class="button" href="' . $button_link . '">' . $button_text . '</a>';
return $button;
}
}

*/

/*
add_shortcode( 'bt_home_page_products', function() {
    ob_start();
    bt_render_home_page_products ();
    return ob_get_clean();
});



function bt_render_home_page_products() {
    
    $args = array(
        'taxonomy'   => "product_cat",
        'number'     => 0,
        'hide_empty' => true,
		'meta_key' => 'homepage_sorting',
        'meta_compare' => '>',
        'meta_value' => '0',
        'orderby' => 'meta_value_num',
        'order' => 'ASC'
    );
	
    $product_categories = get_terms($args);
	echo '<section class="bt_cat_grid">';
    foreach ($product_categories as $category) {
        $products = wc_get_products(array(
            'status' => array('publish'),
            'category' => array($category->slug),
            'limit' => -1,
        ));

        if (empty($products)) {continue;}
        
       
        foreach ( $products as $product ) :
            $post_object = get_post( $product->get_id() );
            setup_postdata( $GLOBALS['post'] =& $post_object );
            wc_get_template_part( 'content', 'product' );
        endforeach;
        wp_reset_postdata();
    }
	  echo '</section>'; // end bt_cat_grid
}

/**
 * @snippet       Move Email Field To Top @ Checkout Page
 */
  
add_filter( 'woocommerce_billing_fields', 'bbloomer_move_checkout_email_field' );
 
function bbloomer_move_checkout_email_field( $address_fields ) {
    $address_fields['billing_email']['priority'] = 1;
    return $address_fields;
}

// add_action('shutdown', function(){
// 	if (current_user_can('administrator')){
//         trigger_error("roi test" . WP_CONTENT_DIR);
// 		ob_start();
// 		global $wpdb;
// 		print_r($wpdb->queries);
// 		$results=ob_get_clean();
// 		file_put_contents(WP_CONTENT_DIR.'/queries.txt', date('y-m-d H:i:s')." Path: $_SERVER[REQUEST_URI] \n".PHP_EOL.$results, FILE_APPEND);
// 	}
// });