<?php

add_shortcode( 'bt_shop_page', function() {
    ob_start();
    bt_render_shop_page();
    return ob_get_clean();
});

function bt_render_shop_filter($attribute_slug, $current_filter) {
    if (empty($attribute_slug)) {
        return;
    }
    $terms = get_terms($attribute_slug);
    if (empty($terms)) {
        return;
    }
    global $wp;
    $current_page = home_url( $wp->request );
    $filter_title = jet_engine()->listings->data->get_option( 'bt-settings::bt_filter_title' )
    ?>
        <div class="bt_filter">
            <?=$filter_title?>
            <div class="bt_filter_items">
            <?php
            $active_class = empty($current_filter) ? 'active' : '';
            echo "<a class='bt_filter_item $active_class' href='$current_page'>".__('הכל', 'bunnytummy').'</a>';
            foreach ($terms as $term) {
                $filter = $current_filter;
                $active_class = '';
                $slug = urldecode($term->slug);
                if (in_array($slug, $current_filter)) {
                    $filter = array_diff($current_filter, [$slug]);
                    $active_class = 'active';
                } else {
                    array_push($filter, $slug);
                }
                $new_filter_str = implode(',', $filter);
                $new_page = add_query_arg("bt_$attribute_slug", $new_filter_str, $current_page);
                // print_r($new_page);
                echo "<a class='bt_filter_item $active_class' href='$new_page'>".$term->name.'</a>';
            }
            ?>
            </div>
        </div>
    <?php
    // print_r($terms);
}
function bt_render_shop_page() {
    $attribute_slug = jet_engine()->listings->data->get_option( 'bt-settings::bt_filter_by' );

    $current_filter_str = $_GET["bt_$attribute_slug"] ?? '';
    $current_filter = empty($current_filter_str) ? [] : explode(',', $current_filter_str);
    $tax_query = empty($current_filter) ? null : array( array(
        'taxonomy'        => $attribute_slug,
        'field'           => 'slug',
        'terms'           =>  $current_filter,
        'operator'        => 'IN',
    ) );
    echo '<div class="bt_shop">';
    bt_render_shop_filter($attribute_slug, $current_filter);
    
    $args = array(
        'taxonomy'   => "product_cat",
        'number'     => 0,
        'hide_empty' => true,
    );
    $product_categories = get_terms($args);
    foreach ($product_categories as $category) {
        $cat_thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);
        $cat_image = wp_get_attachment_url( $cat_thumbnail_id );
        $cat_link = get_term_link( $category );
        $designed_title = get_term_meta( $category->term_id, 'designed-title', true);
        $products = wc_get_products(array(
            'status' => array('publish'),
            'category' => array($category->slug),
            'limit' => -1,
            'tax_query' => $tax_query,
            'meta_key'  => 'archive_sorting',
            'orderby'   => 'meta_value_num'
        ));

        if (empty($products)) {continue;}
        
        echo '<section class="bt_cat_grid">';
        echo   "<a class='bt_cat_tile' href='$cat_link'>";
        echo    "<img class='bt_cat_img' src='$cat_image' />";
        echo    "<div class='bt_cat_title' >{$designed_title}</div>";
        echo   "</a>";

        foreach ( $products as $product ) :
            $post_object = get_post( $product->get_id() );
            setup_postdata( $GLOBALS['post'] =& $post_object );
            wc_get_template_part( 'content', 'product' );
        endforeach;
        wp_reset_postdata();
        echo '</section>'; // end bt_cat_grid
    }
    echo '</div>'; // end bt_shop
}


add_shortcode( 'bt_measure_content', function() {
    ob_start();
    bt_render_measure_content();
    return ob_get_clean();
});


function bt_render_measure_content() {
    global $product;
    if (!$product || !$product->is_type('variable')) {
        return 'Not a variable product';
    }
    $variations = get_post_meta( $product->get_id(), 'measure_variations', true );
    if (empty($variations)) {return;}

    ?>
    <div class="bt_measure_wrapper">
        <section class="measure_section select">
            <div class='measure_title'><?= __('מראה הדגם במידות נוספות', 'bunnytummy')?></div>
            <div class='measure_subtitle'><?= __('בחרי את המידה כדי לראות את הדגם במידה שלך', 'bunnytummy')?></div>
            <div class='measure_list'>
                <?php
                foreach ($variations as $key => $variation) {
                    $active_class = $key === array_key_first($variations) ? 'bt-active' : '';
                    echo "<button class='measure_btn $active_class' data-item-id='$key'>{$variation['size']}</button>";
                }
                ?>
            </div>
        </section>
        <section class="measure_section view">
                <?php
                foreach ($variations as $key => $variation) {
                    $active_class = $key === array_key_first($variations) ? 'bt-active' : '';
                    ?>
                    <div class="measure_view_item <?=$active_class?>" data-item-id="<?=$key?>">
                        <img class="measure_image" src="<?= wp_get_attachment_image_src( $variation['image'], 'large')[0]?>" />
                        <div class="measure_description"><?= $variation['description'] ?></div>
                    </div>
                    <?php
                }
                ?>
        </section>
    </div>
    <?php
}