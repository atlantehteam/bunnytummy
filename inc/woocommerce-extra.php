<?php
/*
add_action( 'woocommerce_product_after_variable_attributes', 'variation_settings_fields', 10, 3 );
add_action( 'woocommerce_save_product_variation', 'save_variation_settings_fields', 10, 2 );

function variation_settings_fields( $loop, $variation_data, $variation ) {
    $measure_image_id = $variation_data['measure_image_id'][0] ?? null;
    ?>
    <section>
        <h3>צילום הוריאציה</h3>
        <p class="form-row form-row-first upload_measure_image">
            <a
                href="#"
                class="upload_measure_image_button tips <?php echo $measure_image_id ? 'remove' : ''; ?>"
                data-tip="<?php echo $measure_image_id ? esc_attr__( 'Remove this image', 'woocommerce' ) : esc_attr__( 'Upload an image', 'woocommerce' ); ?>"
                rel="<?php echo esc_attr( $variation->ID ); ?>">
                <img src="<?php echo $measure_image_id ? esc_url( wp_get_attachment_thumb_url( $measure_image_id ) ) : esc_url( wc_placeholder_img_src() ); ?>" />
                <input
                    type="hidden"
                    name="upload_measure_image_id[<?php echo esc_attr( $loop ); ?>]"
                    class="upload_measure_image_id" value="<?php echo esc_attr( $measure_image_id ); ?>" />
            </a>
        </p>
        <?php
        woocommerce_wp_text_input(
            array(
                'id'            => "measure_description{$loop}",
                'name'          => "measure_description[{$loop}]",
                'value'         => get_post_meta( $variation->ID, 'measure_description', true ),
                'label'         => __( 'הסבר על צילום הוריאציה', 'woocommerce' ),
                'desc_tip'      => true,
                'wrapper_class' => 'form-row form-row-full',
            )
        );
        ?>
    </section>
    <?php
}

function save_variation_settings_fields( $variation_id, $loop ) {
    if (isset( $_POST['upload_measure_image_id'][ $loop ] )) {
        $value = wc_clean( wp_unslash( $_POST['upload_measure_image_id'][ $loop ] ) );
        update_post_meta( $variation_id, 'measure_image_id', esc_attr( $value ));
    }
    if (isset( $_POST['measure_description'][ $loop ] )) {
        $value = wc_clean( wp_unslash( $_POST['measure_description'][ $loop ] ) );
        update_post_meta( $variation_id, 'measure_description', esc_attr( $value ));
    }
}
*/

add_filter( 'woocommerce_get_item_data', 'bt_cart_color_filter', 10, 2);
function bt_cart_color_filter($item_data, $cart_item) {
    if (!isset($cart_item['variation']['attribute_pa_color'])) {
        return $item_data;
    }

    $term = get_term_by('slug', $cart_item['variation']['attribute_pa_color'], 'pa_color');
    if (!$term) {
        return $item_data;
    }
    $color = get_term_meta($term->term_id, 'product_attribute_color', true);
    if (!$color) {
        return $item_data;
    }

    $taxonomy = get_taxonomy( 'pa_color' );
    $taxonomy_label = $taxonomy->labels->singular_name;

    foreach ($item_data as &$attribute) {
        if ($attribute['key'] == $taxonomy_label) {
            $attribute['display'] = $attribute['value'] . "<div><span class='bt_attr_color' style='background-color: $color'></span></div>";
        }
    }
    return $item_data;
}

add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_show_page_title', '__return_false' );
